module.exports = {
  handleErr (err, res, msg, code) {
    console.log(JSON.stringify(err, null, 2))
    res.status(code || 500).send(msg || 'there was error')
  },
  stringify (obj) {
    return JSON.stringify(obj, null, 2)
  }
}
