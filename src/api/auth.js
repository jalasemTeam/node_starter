const router = require('express').Router()
const config = require('../config')
const bcrypt = require('bcrypt')
const AuthPolicy = require('../policies/AuthPolicy')
const utils = require('../utils')

const Artist = require('../model/artist')
const Member = require('../model/member')
var jwt = require('jsonwebtoken')

router.post('/auth', AuthPolicy.login, (req, res) => {
  let {email, password, userType} = req.body
  let pass = password
  if (userType === 'artist') {
    Artist.findOne({email}, 'password profilePic firstname middlename lastname fullname email', (err, user) => {
      if (err) {
        utils.handleErr(err, res, 'invalid login details', 403)
      } else if (user === null) {
        utils.handleErr(err, res, 'user does not exist', 403)
      } else {
        let {email, firstname, middlename, surname, fullname, userType, profilePic} = user
        if (bcrypt.compareSync(pass, user.password)) {
          const payload = {
            email,
            auth_id: user._id,
            login_type: userType
          }
          let token = jwt.sign(payload, config.token_secret)
          res.status(200).send({token, email, firstname, middlename, surname, fullname, userType: 'artist', profilePic})
        } else {
          res.status(403).send('incorrect login and password combination!')
        }
      }
    })
  } else {
    Member.findOne({email}, 'password profilePic firstname middlename lastname fullname email', (err, user) => {
      if (err) {
        utils.handleErr(err, res, 'invalid login details', 403)
      } else if (user === null) {
        utils.handleErr(err, res, 'user does not exist', 403)
      } else {
        let {email, firstname, middlename, surname, fullname, userType, profilePic} = user
        if (bcrypt.compareSync(pass, user.password)) {
          const payload = {
            email,
            auth_id: user._id,
            login_type: userType
          }
          let token = jwt.sign(payload, config.token_secret)
          req.session.user = token
          res.status(200).send({token, email, firstname, middlename, surname, fullname, userType: 'member', profilePic})
        } else {
          res.status(403).send('incorrect login and password combination!')
        }
      }
    })
  }
})

router.post('/signup/artist', AuthPolicy.registerA, (req, res) => {
  let userData = req.body
  userData.password = bcrypt.hashSync(req.body.password, 4)

  let newArtist = new Artist(userData)
  newArtist.save(err => {
    console.log(JSON.stringify(err, null, 2))
    if (!err) {
      console.log('artist signup successful')
      return res.status(201).send('signup successful')
    } else {
      if (err.code === 11000) {
        console.log('artist already exist')
        return res.status(409).send('artist already exist!')
      } else {
        console.log(JSON.stringify(err, null, 2))
        return res.status(500).send('there were some errors signing up. We are duly notified and we are working hard to fix this.')
      }
    }
  })
})

router.post('/signup/member', AuthPolicy.registerM, (req, res) => {
  console.log(JSON.stringify(req.body, null, 2))
  let userData = req.body
  userData.password = bcrypt.hashSync(req.body.password, 5)

  let newMember = new Member(userData)
  newMember.save(err => {
    console.log(JSON.stringify(err, null, 2))
    if (!err) {
      console.log('user signup successful')
      return res.status(201).send('signup successful')
    } else {
      if (err.code === 11000) {
        console.log('user already exist')
        return res.status(409).send('user already exist!')
      } else {
        console.log(JSON.stringify(err, null, 2))
        return res.status(500).send('there were some errors signing up. We are duly notified and we are working hard to fix this.')
      }
    }
  })
})

router.post('/logout', (req, res) => {
  req.session.destroy(err => {
    if (!err) {
      res.status(200).send('logout successful')
    } else {
      res.status(500).send('there was an error logging out')
    }
  })
})

module.exports = router
