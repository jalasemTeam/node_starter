const mongoose = require('mongoose')
const crypto = require('crypto')
const { Schema } = require('mongoose')
const {ObjectId} = Schema.Types

let artistSchema = new Schema({
  firstname: {
    type: String,
    required: false,
    trim: true
  },
  middlename: {
    type: String,
    required: false,
    trim: true
  },
  lastname: {
    type: String,
    required: false,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    lowercase: true
  },
  stagename: String,
  profilePic: String,
  address: String,
  addressDetails: {
    place_id: String,
    geometry: {
      lat: Number,
      lng: Number
    },
    details: {
      formatted: String,
      lga: String,
      city: String
    }
  },
  tickets: [{
    type: ObjectId,
    ref: 'Tickets'
  }],
  active: Boolean,
  subscribed: {
    status: Boolean,
    subID: ObjectId
  },
  social: {
    facbook: String,
    instagram: String,
    twitter: String
  },
  metrics: {
    views: Number,
    fans: Number,
    videos: Number,
    audio: Number,
    concerts: Number
  },
  servicesUsed: [{
    type: ObjectId,
    ref: 'Services'
  }],
  password: {
    type: String,
    required: true
  },
  passResetKey: String,
  city: String,
  house_address: String,
  country: {
    type: String,
    required: false
  },
  gender: {
    type: String,
    required: true
  },
  date_of_birth: {
    type: Number,
    required: false
  },
  createdOn: {
    type: Date,
    required: false
  },
  disabled: {
    type: Boolean,
    default: false
  },
  updatedAt: {
    type: Number,
    required: false
  },
  last_login: {
    type: Number,
    required: false
  }
}, {runSettersOnQuery: true})

artistSchema.virtual('fullname', () => {
  return `${this.firstname} ${this.middleName || ''} ${this.surname}`
})

artistSchema.pre('save', function (next) {
  let hash = crypto
    .createHash('md5')
    .update(this.email)
    .digest('hex')
  this.profilePic = `https://www.gravatar.com/avatar/${hash}`

  var currentDate = new Date().getTime()
  this.updatedAt = currentDate
  if (!this.createdOn) {
    this.createdOn = currentDate
  }
  next()
})

var artist = mongoose.model('artist', artistSchema)

module.exports = artist
